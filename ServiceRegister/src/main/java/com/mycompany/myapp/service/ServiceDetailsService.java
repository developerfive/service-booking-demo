package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.ServiceDetails;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link ServiceDetails}.
 */
public interface ServiceDetailsService {
    /**
     * Save a serviceDetails.
     *
     * @param serviceDetails the entity to save.
     * @return the persisted entity.
     */
    ServiceDetails save(ServiceDetails serviceDetails);

    /**
     * Partially updates a serviceDetails.
     *
     * @param serviceDetails the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ServiceDetails> partialUpdate(ServiceDetails serviceDetails);

    /**
     * Get all the serviceDetails.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ServiceDetails> findAll(Pageable pageable);

    /**
     * Get the "id" serviceDetails.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ServiceDetails> findOne(Long id);

    /**
     * Delete the "id" serviceDetails.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
