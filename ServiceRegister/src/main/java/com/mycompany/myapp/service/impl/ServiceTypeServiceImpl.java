package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.domain.ServiceType;
import com.mycompany.myapp.repository.ServiceTypeRepository;
import com.mycompany.myapp.service.ServiceTypeService;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ServiceType}.
 */
@Service
@Transactional
public class ServiceTypeServiceImpl implements ServiceTypeService {

    private final Logger log = LoggerFactory.getLogger(ServiceTypeServiceImpl.class);

    private final ServiceTypeRepository serviceTypeRepository;

    public ServiceTypeServiceImpl(ServiceTypeRepository serviceTypeRepository) {
        this.serviceTypeRepository = serviceTypeRepository;
    }

    @Override
    public ServiceType save(ServiceType serviceType) {
        log.debug("Request to save ServiceType : {}", serviceType);
        return serviceTypeRepository.save(serviceType);
    }

    @Override
    public Optional<ServiceType> partialUpdate(ServiceType serviceType) {
        log.debug("Request to partially update ServiceType : {}", serviceType);

        return serviceTypeRepository
            .findById(serviceType.getId())
            .map(existingServiceType -> {
                if (serviceType.getServiceTypeName() != null) {
                    existingServiceType.setServiceTypeName(serviceType.getServiceTypeName());
                }

                return existingServiceType;
            })
            .map(serviceTypeRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ServiceType> findAll(Pageable pageable) {
        log.debug("Request to get all ServiceTypes");
        return serviceTypeRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ServiceType> findOne(Long id) {
        log.debug("Request to get ServiceType : {}", id);
        return serviceTypeRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ServiceType : {}", id);
        serviceTypeRepository.deleteById(id);
    }
}
