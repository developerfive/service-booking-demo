package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.domain.ServiceDetails;
import com.mycompany.myapp.repository.ServiceDetailsRepository;
import com.mycompany.myapp.service.ServiceDetailsService;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ServiceDetails}.
 */
@Service
@Transactional
public class ServiceDetailsServiceImpl implements ServiceDetailsService {

    private final Logger log = LoggerFactory.getLogger(ServiceDetailsServiceImpl.class);

    private final ServiceDetailsRepository serviceDetailsRepository;

    public ServiceDetailsServiceImpl(ServiceDetailsRepository serviceDetailsRepository) {
        this.serviceDetailsRepository = serviceDetailsRepository;
    }

    @Override
    public ServiceDetails save(ServiceDetails serviceDetails) {
        log.debug("Request to save ServiceDetails : {}", serviceDetails);
        return serviceDetailsRepository.save(serviceDetails);
    }

    @Override
    public Optional<ServiceDetails> partialUpdate(ServiceDetails serviceDetails) {
        log.debug("Request to partially update ServiceDetails : {}", serviceDetails);

        return serviceDetailsRepository
            .findById(serviceDetails.getId())
            .map(existingServiceDetails -> {
                if (serviceDetails.getComments() != null) {
                    existingServiceDetails.setComments(serviceDetails.getComments());
                }

                return existingServiceDetails;
            })
            .map(serviceDetailsRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ServiceDetails> findAll(Pageable pageable) {
        log.debug("Request to get all ServiceDetails");
        return serviceDetailsRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ServiceDetails> findOne(Long id) {
        log.debug("Request to get ServiceDetails : {}", id);
        return serviceDetailsRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ServiceDetails : {}", id);
        serviceDetailsRepository.deleteById(id);
    }
}
