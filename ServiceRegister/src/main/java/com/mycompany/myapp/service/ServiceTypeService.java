package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.ServiceType;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link ServiceType}.
 */
public interface ServiceTypeService {
    /**
     * Save a serviceType.
     *
     * @param serviceType the entity to save.
     * @return the persisted entity.
     */
    ServiceType save(ServiceType serviceType);

    /**
     * Partially updates a serviceType.
     *
     * @param serviceType the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ServiceType> partialUpdate(ServiceType serviceType);

    /**
     * Get all the serviceTypes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ServiceType> findAll(Pageable pageable);

    /**
     * Get the "id" serviceType.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ServiceType> findOne(Long id);

    /**
     * Delete the "id" serviceType.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
