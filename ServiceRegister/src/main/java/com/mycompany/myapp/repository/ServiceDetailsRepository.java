package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.ServiceDetails;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the ServiceDetails entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ServiceDetailsRepository extends JpaRepository<ServiceDetails, Long> {}
