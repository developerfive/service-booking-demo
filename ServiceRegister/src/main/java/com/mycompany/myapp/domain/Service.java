package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Service.
 */
@Entity
@Table(name = "service")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Service implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "service_name")
    private String serviceName;

    @OneToMany(mappedBy = "serviceId")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "serviceId" }, allowSetters = true)
    private Set<ServiceDetails> serviceDetails = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "services" }, allowSetters = true)
    private ServiceType serviceTypeId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Service id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getServiceName() {
        return this.serviceName;
    }

    public Service serviceName(String serviceName) {
        this.setServiceName(serviceName);
        return this;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public Set<ServiceDetails> getServiceDetails() {
        return this.serviceDetails;
    }

    public void setServiceDetails(Set<ServiceDetails> serviceDetails) {
        if (this.serviceDetails != null) {
            this.serviceDetails.forEach(i -> i.setServiceId(null));
        }
        if (serviceDetails != null) {
            serviceDetails.forEach(i -> i.setServiceId(this));
        }
        this.serviceDetails = serviceDetails;
    }

    public Service serviceDetails(Set<ServiceDetails> serviceDetails) {
        this.setServiceDetails(serviceDetails);
        return this;
    }

    public Service addServiceDetails(ServiceDetails serviceDetails) {
        this.serviceDetails.add(serviceDetails);
        serviceDetails.setServiceId(this);
        return this;
    }

    public Service removeServiceDetails(ServiceDetails serviceDetails) {
        this.serviceDetails.remove(serviceDetails);
        serviceDetails.setServiceId(null);
        return this;
    }

    public ServiceType getServiceTypeId() {
        return this.serviceTypeId;
    }

    public void setServiceTypeId(ServiceType serviceType) {
        this.serviceTypeId = serviceType;
    }

    public Service serviceTypeId(ServiceType serviceType) {
        this.setServiceTypeId(serviceType);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Service)) {
            return false;
        }
        return id != null && id.equals(((Service) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Service{" +
            "id=" + getId() +
            ", serviceName='" + getServiceName() + "'" +
            "}";
    }
}
