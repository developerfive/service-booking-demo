package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.ServiceType;
import com.mycompany.myapp.repository.ServiceTypeRepository;
import com.mycompany.myapp.service.ServiceTypeService;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.ServiceType}.
 */
@RestController
@RequestMapping("/api")
public class ServiceTypeResource {

    private final Logger log = LoggerFactory.getLogger(ServiceTypeResource.class);

    private static final String ENTITY_NAME = "serviceType";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ServiceTypeService serviceTypeService;

    private final ServiceTypeRepository serviceTypeRepository;

    public ServiceTypeResource(ServiceTypeService serviceTypeService, ServiceTypeRepository serviceTypeRepository) {
        this.serviceTypeService = serviceTypeService;
        this.serviceTypeRepository = serviceTypeRepository;
    }

    /**
     * {@code POST  /service-types} : Create a new serviceType.
     *
     * @param serviceType the serviceType to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new serviceType, or with status {@code 400 (Bad Request)} if the serviceType has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/service-types")
    public ResponseEntity<ServiceType> createServiceType(@RequestBody ServiceType serviceType) throws URISyntaxException {
        log.debug("REST request to save ServiceType : {}", serviceType);
        if (serviceType.getId() != null) {
            throw new BadRequestAlertException("A new serviceType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ServiceType result = serviceTypeService.save(serviceType);
        return ResponseEntity
            .created(new URI("/api/service-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /service-types/:id} : Updates an existing serviceType.
     *
     * @param id the id of the serviceType to save.
     * @param serviceType the serviceType to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated serviceType,
     * or with status {@code 400 (Bad Request)} if the serviceType is not valid,
     * or with status {@code 500 (Internal Server Error)} if the serviceType couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/service-types/{id}")
    public ResponseEntity<ServiceType> updateServiceType(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ServiceType serviceType
    ) throws URISyntaxException {
        log.debug("REST request to update ServiceType : {}, {}", id, serviceType);
        if (serviceType.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, serviceType.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!serviceTypeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ServiceType result = serviceTypeService.save(serviceType);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, serviceType.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /service-types/:id} : Partial updates given fields of an existing serviceType, field will ignore if it is null
     *
     * @param id the id of the serviceType to save.
     * @param serviceType the serviceType to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated serviceType,
     * or with status {@code 400 (Bad Request)} if the serviceType is not valid,
     * or with status {@code 404 (Not Found)} if the serviceType is not found,
     * or with status {@code 500 (Internal Server Error)} if the serviceType couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/service-types/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ServiceType> partialUpdateServiceType(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ServiceType serviceType
    ) throws URISyntaxException {
        log.debug("REST request to partial update ServiceType partially : {}, {}", id, serviceType);
        if (serviceType.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, serviceType.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!serviceTypeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ServiceType> result = serviceTypeService.partialUpdate(serviceType);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, serviceType.getId().toString())
        );
    }

    /**
     * {@code GET  /service-types} : get all the serviceTypes.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of serviceTypes in body.
     */
    @GetMapping("/service-types")
    public ResponseEntity<List<ServiceType>> getAllServiceTypes(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of ServiceTypes");
        Page<ServiceType> page = serviceTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /service-types/:id} : get the "id" serviceType.
     *
     * @param id the id of the serviceType to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the serviceType, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/service-types/{id}")
    public ResponseEntity<ServiceType> getServiceType(@PathVariable Long id) {
        log.debug("REST request to get ServiceType : {}", id);
        Optional<ServiceType> serviceType = serviceTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(serviceType);
    }

    /**
     * {@code DELETE  /service-types/:id} : delete the "id" serviceType.
     *
     * @param id the id of the serviceType to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/service-types/{id}")
    public ResponseEntity<Void> deleteServiceType(@PathVariable Long id) {
        log.debug("REST request to delete ServiceType : {}", id);
        serviceTypeService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
