package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.ServiceDetails;
import com.mycompany.myapp.repository.ServiceDetailsRepository;
import com.mycompany.myapp.service.ServiceDetailsService;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.ServiceDetails}.
 */
@RestController
@RequestMapping("/api")
public class ServiceDetailsResource {

    private final Logger log = LoggerFactory.getLogger(ServiceDetailsResource.class);

    private static final String ENTITY_NAME = "serviceDetails";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ServiceDetailsService serviceDetailsService;

    private final ServiceDetailsRepository serviceDetailsRepository;

    public ServiceDetailsResource(ServiceDetailsService serviceDetailsService, ServiceDetailsRepository serviceDetailsRepository) {
        this.serviceDetailsService = serviceDetailsService;
        this.serviceDetailsRepository = serviceDetailsRepository;
    }

    /**
     * {@code POST  /service-details} : Create a new serviceDetails.
     *
     * @param serviceDetails the serviceDetails to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new serviceDetails, or with status {@code 400 (Bad Request)} if the serviceDetails has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/service-details")
    public ResponseEntity<ServiceDetails> createServiceDetails(@RequestBody ServiceDetails serviceDetails) throws URISyntaxException {
        log.debug("REST request to save ServiceDetails : {}", serviceDetails);
        if (serviceDetails.getId() != null) {
            throw new BadRequestAlertException("A new serviceDetails cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ServiceDetails result = serviceDetailsService.save(serviceDetails);
        return ResponseEntity
            .created(new URI("/api/service-details/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /service-details/:id} : Updates an existing serviceDetails.
     *
     * @param id the id of the serviceDetails to save.
     * @param serviceDetails the serviceDetails to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated serviceDetails,
     * or with status {@code 400 (Bad Request)} if the serviceDetails is not valid,
     * or with status {@code 500 (Internal Server Error)} if the serviceDetails couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/service-details/{id}")
    public ResponseEntity<ServiceDetails> updateServiceDetails(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ServiceDetails serviceDetails
    ) throws URISyntaxException {
        log.debug("REST request to update ServiceDetails : {}, {}", id, serviceDetails);
        if (serviceDetails.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, serviceDetails.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!serviceDetailsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ServiceDetails result = serviceDetailsService.save(serviceDetails);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, serviceDetails.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /service-details/:id} : Partial updates given fields of an existing serviceDetails, field will ignore if it is null
     *
     * @param id the id of the serviceDetails to save.
     * @param serviceDetails the serviceDetails to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated serviceDetails,
     * or with status {@code 400 (Bad Request)} if the serviceDetails is not valid,
     * or with status {@code 404 (Not Found)} if the serviceDetails is not found,
     * or with status {@code 500 (Internal Server Error)} if the serviceDetails couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/service-details/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ServiceDetails> partialUpdateServiceDetails(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ServiceDetails serviceDetails
    ) throws URISyntaxException {
        log.debug("REST request to partial update ServiceDetails partially : {}, {}", id, serviceDetails);
        if (serviceDetails.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, serviceDetails.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!serviceDetailsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ServiceDetails> result = serviceDetailsService.partialUpdate(serviceDetails);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, serviceDetails.getId().toString())
        );
    }

    /**
     * {@code GET  /service-details} : get all the serviceDetails.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of serviceDetails in body.
     */
    @GetMapping("/service-details")
    public ResponseEntity<List<ServiceDetails>> getAllServiceDetails(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of ServiceDetails");
        Page<ServiceDetails> page = serviceDetailsService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /service-details/:id} : get the "id" serviceDetails.
     *
     * @param id the id of the serviceDetails to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the serviceDetails, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/service-details/{id}")
    public ResponseEntity<ServiceDetails> getServiceDetails(@PathVariable Long id) {
        log.debug("REST request to get ServiceDetails : {}", id);
        Optional<ServiceDetails> serviceDetails = serviceDetailsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(serviceDetails);
    }

    /**
     * {@code DELETE  /service-details/:id} : delete the "id" serviceDetails.
     *
     * @param id the id of the serviceDetails to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/service-details/{id}")
    public ResponseEntity<Void> deleteServiceDetails(@PathVariable Long id) {
        log.debug("REST request to delete ServiceDetails : {}", id);
        serviceDetailsService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
