import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'service',
        data: { pageTitle: 'Services' },
        loadChildren: () => import('./service/service.module').then(m => m.ServiceModule),
      },
      {
        path: 'service-type',
        data: { pageTitle: 'ServiceTypes' },
        loadChildren: () => import('./service-type/service-type.module').then(m => m.ServiceTypeModule),
      },
      {
        path: 'service-details',
        data: { pageTitle: 'ServiceDetails' },
        loadChildren: () => import('./service-details/service-details.module').then(m => m.ServiceDetailsModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
