import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { ServiceDetailsComponent } from './list/service-details.component';
import { ServiceDetailsDetailComponent } from './detail/service-details-detail.component';
import { ServiceDetailsUpdateComponent } from './update/service-details-update.component';
import { ServiceDetailsDeleteDialogComponent } from './delete/service-details-delete-dialog.component';
import { ServiceDetailsRoutingModule } from './route/service-details-routing.module';

@NgModule({
  imports: [SharedModule, ServiceDetailsRoutingModule],
  declarations: [
    ServiceDetailsComponent,
    ServiceDetailsDetailComponent,
    ServiceDetailsUpdateComponent,
    ServiceDetailsDeleteDialogComponent,
  ],
  entryComponents: [ServiceDetailsDeleteDialogComponent],
})
export class ServiceDetailsModule {}
