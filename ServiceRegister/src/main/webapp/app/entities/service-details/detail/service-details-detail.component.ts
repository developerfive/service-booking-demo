import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IServiceDetails } from '../service-details.model';

@Component({
  selector: 'jhi-service-details-detail',
  templateUrl: './service-details-detail.component.html',
})
export class ServiceDetailsDetailComponent implements OnInit {
  serviceDetails: IServiceDetails | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ serviceDetails }) => {
      this.serviceDetails = serviceDetails;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
