import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ServiceDetailsDetailComponent } from './service-details-detail.component';

describe('ServiceDetails Management Detail Component', () => {
  let comp: ServiceDetailsDetailComponent;
  let fixture: ComponentFixture<ServiceDetailsDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ServiceDetailsDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ serviceDetails: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(ServiceDetailsDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(ServiceDetailsDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load serviceDetails on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.serviceDetails).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
