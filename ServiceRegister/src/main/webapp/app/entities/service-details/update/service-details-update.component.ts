import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IServiceDetails, ServiceDetails } from '../service-details.model';
import { ServiceDetailsService } from '../service/service-details.service';
import { IService } from 'app/entities/service/service.model';
import { ServiceService } from 'app/entities/service/service/service.service';

@Component({
  selector: 'jhi-service-details-update',
  templateUrl: './service-details-update.component.html',
})
export class ServiceDetailsUpdateComponent implements OnInit {
  isSaving = false;

  servicesSharedCollection: IService[] = [];

  editForm = this.fb.group({
    id: [],
    comments: [],
    serviceId: [],
  });

  constructor(
    protected serviceDetailsService: ServiceDetailsService,
    protected serviceService: ServiceService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ serviceDetails }) => {
      this.updateForm(serviceDetails);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const serviceDetails = this.createFromForm();
    if (serviceDetails.id !== undefined) {
      this.subscribeToSaveResponse(this.serviceDetailsService.update(serviceDetails));
    } else {
      this.subscribeToSaveResponse(this.serviceDetailsService.create(serviceDetails));
    }
  }

  trackServiceById(index: number, item: IService): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IServiceDetails>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(serviceDetails: IServiceDetails): void {
    this.editForm.patchValue({
      id: serviceDetails.id,
      comments: serviceDetails.comments,
      serviceId: serviceDetails.serviceId,
    });

    this.servicesSharedCollection = this.serviceService.addServiceToCollectionIfMissing(
      this.servicesSharedCollection,
      serviceDetails.serviceId
    );
  }

  protected loadRelationshipsOptions(): void {
    this.serviceService
      .query()
      .pipe(map((res: HttpResponse<IService[]>) => res.body ?? []))
      .pipe(
        map((services: IService[]) => this.serviceService.addServiceToCollectionIfMissing(services, this.editForm.get('serviceId')!.value))
      )
      .subscribe((services: IService[]) => (this.servicesSharedCollection = services));
  }

  protected createFromForm(): IServiceDetails {
    return {
      ...new ServiceDetails(),
      id: this.editForm.get(['id'])!.value,
      comments: this.editForm.get(['comments'])!.value,
      serviceId: this.editForm.get(['serviceId'])!.value,
    };
  }
}
