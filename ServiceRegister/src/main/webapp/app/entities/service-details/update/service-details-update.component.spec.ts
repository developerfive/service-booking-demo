import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { ServiceDetailsService } from '../service/service-details.service';
import { IServiceDetails, ServiceDetails } from '../service-details.model';
import { IService } from 'app/entities/service/service.model';
import { ServiceService } from 'app/entities/service/service/service.service';

import { ServiceDetailsUpdateComponent } from './service-details-update.component';

describe('ServiceDetails Management Update Component', () => {
  let comp: ServiceDetailsUpdateComponent;
  let fixture: ComponentFixture<ServiceDetailsUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let serviceDetailsService: ServiceDetailsService;
  let serviceService: ServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [ServiceDetailsUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(ServiceDetailsUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ServiceDetailsUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    serviceDetailsService = TestBed.inject(ServiceDetailsService);
    serviceService = TestBed.inject(ServiceService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Service query and add missing value', () => {
      const serviceDetails: IServiceDetails = { id: 456 };
      const serviceId: IService = { id: 10881 };
      serviceDetails.serviceId = serviceId;

      const serviceCollection: IService[] = [{ id: 92444 }];
      jest.spyOn(serviceService, 'query').mockReturnValue(of(new HttpResponse({ body: serviceCollection })));
      const additionalServices = [serviceId];
      const expectedCollection: IService[] = [...additionalServices, ...serviceCollection];
      jest.spyOn(serviceService, 'addServiceToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ serviceDetails });
      comp.ngOnInit();

      expect(serviceService.query).toHaveBeenCalled();
      expect(serviceService.addServiceToCollectionIfMissing).toHaveBeenCalledWith(serviceCollection, ...additionalServices);
      expect(comp.servicesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const serviceDetails: IServiceDetails = { id: 456 };
      const serviceId: IService = { id: 86655 };
      serviceDetails.serviceId = serviceId;

      activatedRoute.data = of({ serviceDetails });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(serviceDetails));
      expect(comp.servicesSharedCollection).toContain(serviceId);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ServiceDetails>>();
      const serviceDetails = { id: 123 };
      jest.spyOn(serviceDetailsService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ serviceDetails });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: serviceDetails }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(serviceDetailsService.update).toHaveBeenCalledWith(serviceDetails);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ServiceDetails>>();
      const serviceDetails = new ServiceDetails();
      jest.spyOn(serviceDetailsService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ serviceDetails });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: serviceDetails }));
      saveSubject.complete();

      // THEN
      expect(serviceDetailsService.create).toHaveBeenCalledWith(serviceDetails);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ServiceDetails>>();
      const serviceDetails = { id: 123 };
      jest.spyOn(serviceDetailsService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ serviceDetails });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(serviceDetailsService.update).toHaveBeenCalledWith(serviceDetails);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackServiceById', () => {
      it('Should return tracked Service primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackServiceById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
