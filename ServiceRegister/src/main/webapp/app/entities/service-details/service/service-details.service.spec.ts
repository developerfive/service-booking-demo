import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IServiceDetails, ServiceDetails } from '../service-details.model';

import { ServiceDetailsService } from './service-details.service';

describe('ServiceDetails Service', () => {
  let service: ServiceDetailsService;
  let httpMock: HttpTestingController;
  let elemDefault: IServiceDetails;
  let expectedResult: IServiceDetails | IServiceDetails[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(ServiceDetailsService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      comments: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a ServiceDetails', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new ServiceDetails()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a ServiceDetails', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          comments: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a ServiceDetails', () => {
      const patchObject = Object.assign({}, new ServiceDetails());

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of ServiceDetails', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          comments: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a ServiceDetails', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addServiceDetailsToCollectionIfMissing', () => {
      it('should add a ServiceDetails to an empty array', () => {
        const serviceDetails: IServiceDetails = { id: 123 };
        expectedResult = service.addServiceDetailsToCollectionIfMissing([], serviceDetails);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(serviceDetails);
      });

      it('should not add a ServiceDetails to an array that contains it', () => {
        const serviceDetails: IServiceDetails = { id: 123 };
        const serviceDetailsCollection: IServiceDetails[] = [
          {
            ...serviceDetails,
          },
          { id: 456 },
        ];
        expectedResult = service.addServiceDetailsToCollectionIfMissing(serviceDetailsCollection, serviceDetails);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a ServiceDetails to an array that doesn't contain it", () => {
        const serviceDetails: IServiceDetails = { id: 123 };
        const serviceDetailsCollection: IServiceDetails[] = [{ id: 456 }];
        expectedResult = service.addServiceDetailsToCollectionIfMissing(serviceDetailsCollection, serviceDetails);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(serviceDetails);
      });

      it('should add only unique ServiceDetails to an array', () => {
        const serviceDetailsArray: IServiceDetails[] = [{ id: 123 }, { id: 456 }, { id: 88874 }];
        const serviceDetailsCollection: IServiceDetails[] = [{ id: 123 }];
        expectedResult = service.addServiceDetailsToCollectionIfMissing(serviceDetailsCollection, ...serviceDetailsArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const serviceDetails: IServiceDetails = { id: 123 };
        const serviceDetails2: IServiceDetails = { id: 456 };
        expectedResult = service.addServiceDetailsToCollectionIfMissing([], serviceDetails, serviceDetails2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(serviceDetails);
        expect(expectedResult).toContain(serviceDetails2);
      });

      it('should accept null and undefined values', () => {
        const serviceDetails: IServiceDetails = { id: 123 };
        expectedResult = service.addServiceDetailsToCollectionIfMissing([], null, serviceDetails, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(serviceDetails);
      });

      it('should return initial array if no ServiceDetails is added', () => {
        const serviceDetailsCollection: IServiceDetails[] = [{ id: 123 }];
        expectedResult = service.addServiceDetailsToCollectionIfMissing(serviceDetailsCollection, undefined, null);
        expect(expectedResult).toEqual(serviceDetailsCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
