import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IServiceDetails, getServiceDetailsIdentifier } from '../service-details.model';

export type EntityResponseType = HttpResponse<IServiceDetails>;
export type EntityArrayResponseType = HttpResponse<IServiceDetails[]>;

@Injectable({ providedIn: 'root' })
export class ServiceDetailsService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/service-details');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(serviceDetails: IServiceDetails): Observable<EntityResponseType> {
    return this.http.post<IServiceDetails>(this.resourceUrl, serviceDetails, { observe: 'response' });
  }

  update(serviceDetails: IServiceDetails): Observable<EntityResponseType> {
    return this.http.put<IServiceDetails>(`${this.resourceUrl}/${getServiceDetailsIdentifier(serviceDetails) as number}`, serviceDetails, {
      observe: 'response',
    });
  }

  partialUpdate(serviceDetails: IServiceDetails): Observable<EntityResponseType> {
    return this.http.patch<IServiceDetails>(
      `${this.resourceUrl}/${getServiceDetailsIdentifier(serviceDetails) as number}`,
      serviceDetails,
      { observe: 'response' }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IServiceDetails>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IServiceDetails[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addServiceDetailsToCollectionIfMissing(
    serviceDetailsCollection: IServiceDetails[],
    ...serviceDetailsToCheck: (IServiceDetails | null | undefined)[]
  ): IServiceDetails[] {
    const serviceDetails: IServiceDetails[] = serviceDetailsToCheck.filter(isPresent);
    if (serviceDetails.length > 0) {
      const serviceDetailsCollectionIdentifiers = serviceDetailsCollection.map(
        serviceDetailsItem => getServiceDetailsIdentifier(serviceDetailsItem)!
      );
      const serviceDetailsToAdd = serviceDetails.filter(serviceDetailsItem => {
        const serviceDetailsIdentifier = getServiceDetailsIdentifier(serviceDetailsItem);
        if (serviceDetailsIdentifier == null || serviceDetailsCollectionIdentifiers.includes(serviceDetailsIdentifier)) {
          return false;
        }
        serviceDetailsCollectionIdentifiers.push(serviceDetailsIdentifier);
        return true;
      });
      return [...serviceDetailsToAdd, ...serviceDetailsCollection];
    }
    return serviceDetailsCollection;
  }
}
