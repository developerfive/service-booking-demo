import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ServiceDetailsComponent } from '../list/service-details.component';
import { ServiceDetailsDetailComponent } from '../detail/service-details-detail.component';
import { ServiceDetailsUpdateComponent } from '../update/service-details-update.component';
import { ServiceDetailsRoutingResolveService } from './service-details-routing-resolve.service';

const serviceDetailsRoute: Routes = [
  {
    path: '',
    component: ServiceDetailsComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ServiceDetailsDetailComponent,
    resolve: {
      serviceDetails: ServiceDetailsRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ServiceDetailsUpdateComponent,
    resolve: {
      serviceDetails: ServiceDetailsRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ServiceDetailsUpdateComponent,
    resolve: {
      serviceDetails: ServiceDetailsRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(serviceDetailsRoute)],
  exports: [RouterModule],
})
export class ServiceDetailsRoutingModule {}
