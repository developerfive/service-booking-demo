import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IServiceDetails, ServiceDetails } from '../service-details.model';
import { ServiceDetailsService } from '../service/service-details.service';

@Injectable({ providedIn: 'root' })
export class ServiceDetailsRoutingResolveService implements Resolve<IServiceDetails> {
  constructor(protected service: ServiceDetailsService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IServiceDetails> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((serviceDetails: HttpResponse<ServiceDetails>) => {
          if (serviceDetails.body) {
            return of(serviceDetails.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ServiceDetails());
  }
}
