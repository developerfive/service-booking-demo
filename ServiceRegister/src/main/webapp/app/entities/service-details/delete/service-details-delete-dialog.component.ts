import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IServiceDetails } from '../service-details.model';
import { ServiceDetailsService } from '../service/service-details.service';

@Component({
  templateUrl: './service-details-delete-dialog.component.html',
})
export class ServiceDetailsDeleteDialogComponent {
  serviceDetails?: IServiceDetails;

  constructor(protected serviceDetailsService: ServiceDetailsService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.serviceDetailsService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
