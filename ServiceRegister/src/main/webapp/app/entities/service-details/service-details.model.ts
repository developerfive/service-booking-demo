import { IService } from 'app/entities/service/service.model';

export interface IServiceDetails {
  id?: number;
  comments?: string | null;
  serviceId?: IService | null;
}

export class ServiceDetails implements IServiceDetails {
  constructor(public id?: number, public comments?: string | null, public serviceId?: IService | null) {}
}

export function getServiceDetailsIdentifier(serviceDetails: IServiceDetails): number | undefined {
  return serviceDetails.id;
}
