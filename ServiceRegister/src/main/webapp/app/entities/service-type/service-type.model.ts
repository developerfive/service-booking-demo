import { IService } from 'app/entities/service/service.model';

export interface IServiceType {
  id?: number;
  serviceTypeName?: string | null;
  services?: IService[] | null;
}

export class ServiceType implements IServiceType {
  constructor(public id?: number, public serviceTypeName?: string | null, public services?: IService[] | null) {}
}

export function getServiceTypeIdentifier(serviceType: IServiceType): number | undefined {
  return serviceType.id;
}
