import { IServiceDetails } from 'app/entities/service-details/service-details.model';
import { IServiceType } from 'app/entities/service-type/service-type.model';

export interface IService {
  id?: number;
  serviceName?: string | null;
  serviceDetails?: IServiceDetails[] | null;
  serviceTypeId?: IServiceType | null;
}

export class Service implements IService {
  constructor(
    public id?: number,
    public serviceName?: string | null,
    public serviceDetails?: IServiceDetails[] | null,
    public serviceTypeId?: IServiceType | null
  ) {}
}

export function getServiceIdentifier(service: IService): number | undefined {
  return service.id;
}
