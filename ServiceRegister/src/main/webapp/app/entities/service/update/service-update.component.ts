import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IService, Service } from '../service.model';
import { ServiceService } from '../service/service.service';
import { IServiceType } from 'app/entities/service-type/service-type.model';
import { ServiceTypeService } from 'app/entities/service-type/service/service-type.service';

@Component({
  selector: 'jhi-service-update',
  templateUrl: './service-update.component.html',
})
export class ServiceUpdateComponent implements OnInit {
  isSaving = false;

  serviceTypesSharedCollection: IServiceType[] = [];

  editForm = this.fb.group({
    id: [],
    serviceName: [],
    serviceTypeId: [],
  });

  constructor(
    protected serviceService: ServiceService,
    protected serviceTypeService: ServiceTypeService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ service }) => {
      this.updateForm(service);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const service = this.createFromForm();
    if (service.id !== undefined) {
      this.subscribeToSaveResponse(this.serviceService.update(service));
    } else {
      this.subscribeToSaveResponse(this.serviceService.create(service));
    }
  }

  trackServiceTypeById(index: number, item: IServiceType): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IService>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(service: IService): void {
    this.editForm.patchValue({
      id: service.id,
      serviceName: service.serviceName,
      serviceTypeId: service.serviceTypeId,
    });

    this.serviceTypesSharedCollection = this.serviceTypeService.addServiceTypeToCollectionIfMissing(
      this.serviceTypesSharedCollection,
      service.serviceTypeId
    );
  }

  protected loadRelationshipsOptions(): void {
    this.serviceTypeService
      .query()
      .pipe(map((res: HttpResponse<IServiceType[]>) => res.body ?? []))
      .pipe(
        map((serviceTypes: IServiceType[]) =>
          this.serviceTypeService.addServiceTypeToCollectionIfMissing(serviceTypes, this.editForm.get('serviceTypeId')!.value)
        )
      )
      .subscribe((serviceTypes: IServiceType[]) => (this.serviceTypesSharedCollection = serviceTypes));
  }

  protected createFromForm(): IService {
    return {
      ...new Service(),
      id: this.editForm.get(['id'])!.value,
      serviceName: this.editForm.get(['serviceName'])!.value,
      serviceTypeId: this.editForm.get(['serviceTypeId'])!.value,
    };
  }
}
