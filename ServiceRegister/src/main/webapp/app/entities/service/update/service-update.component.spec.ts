import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { ServiceService } from '../service/service.service';
import { IService, Service } from '../service.model';
import { IServiceType } from 'app/entities/service-type/service-type.model';
import { ServiceTypeService } from 'app/entities/service-type/service/service-type.service';

import { ServiceUpdateComponent } from './service-update.component';

describe('Service Management Update Component', () => {
  let comp: ServiceUpdateComponent;
  let fixture: ComponentFixture<ServiceUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let serviceService: ServiceService;
  let serviceTypeService: ServiceTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [ServiceUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(ServiceUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ServiceUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    serviceService = TestBed.inject(ServiceService);
    serviceTypeService = TestBed.inject(ServiceTypeService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call ServiceType query and add missing value', () => {
      const service: IService = { id: 456 };
      const serviceTypeId: IServiceType = { id: 96695 };
      service.serviceTypeId = serviceTypeId;

      const serviceTypeCollection: IServiceType[] = [{ id: 91739 }];
      jest.spyOn(serviceTypeService, 'query').mockReturnValue(of(new HttpResponse({ body: serviceTypeCollection })));
      const additionalServiceTypes = [serviceTypeId];
      const expectedCollection: IServiceType[] = [...additionalServiceTypes, ...serviceTypeCollection];
      jest.spyOn(serviceTypeService, 'addServiceTypeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ service });
      comp.ngOnInit();

      expect(serviceTypeService.query).toHaveBeenCalled();
      expect(serviceTypeService.addServiceTypeToCollectionIfMissing).toHaveBeenCalledWith(serviceTypeCollection, ...additionalServiceTypes);
      expect(comp.serviceTypesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const service: IService = { id: 456 };
      const serviceTypeId: IServiceType = { id: 22854 };
      service.serviceTypeId = serviceTypeId;

      activatedRoute.data = of({ service });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(service));
      expect(comp.serviceTypesSharedCollection).toContain(serviceTypeId);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Service>>();
      const service = { id: 123 };
      jest.spyOn(serviceService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ service });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: service }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(serviceService.update).toHaveBeenCalledWith(service);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Service>>();
      const service = new Service();
      jest.spyOn(serviceService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ service });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: service }));
      saveSubject.complete();

      // THEN
      expect(serviceService.create).toHaveBeenCalledWith(service);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Service>>();
      const service = { id: 123 };
      jest.spyOn(serviceService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ service });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(serviceService.update).toHaveBeenCalledWith(service);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackServiceTypeById', () => {
      it('Should return tracked ServiceType primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackServiceTypeById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
