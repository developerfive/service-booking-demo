package com.mycompany.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.ServiceDetails;
import com.mycompany.myapp.repository.ServiceDetailsRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ServiceDetailsResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ServiceDetailsResourceIT {

    private static final String DEFAULT_COMMENTS = "AAAAAAAAAA";
    private static final String UPDATED_COMMENTS = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/service-details";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ServiceDetailsRepository serviceDetailsRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restServiceDetailsMockMvc;

    private ServiceDetails serviceDetails;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ServiceDetails createEntity(EntityManager em) {
        ServiceDetails serviceDetails = new ServiceDetails().comments(DEFAULT_COMMENTS);
        return serviceDetails;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ServiceDetails createUpdatedEntity(EntityManager em) {
        ServiceDetails serviceDetails = new ServiceDetails().comments(UPDATED_COMMENTS);
        return serviceDetails;
    }

    @BeforeEach
    public void initTest() {
        serviceDetails = createEntity(em);
    }

    @Test
    @Transactional
    void createServiceDetails() throws Exception {
        int databaseSizeBeforeCreate = serviceDetailsRepository.findAll().size();
        // Create the ServiceDetails
        restServiceDetailsMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(serviceDetails))
            )
            .andExpect(status().isCreated());

        // Validate the ServiceDetails in the database
        List<ServiceDetails> serviceDetailsList = serviceDetailsRepository.findAll();
        assertThat(serviceDetailsList).hasSize(databaseSizeBeforeCreate + 1);
        ServiceDetails testServiceDetails = serviceDetailsList.get(serviceDetailsList.size() - 1);
        assertThat(testServiceDetails.getComments()).isEqualTo(DEFAULT_COMMENTS);
    }

    @Test
    @Transactional
    void createServiceDetailsWithExistingId() throws Exception {
        // Create the ServiceDetails with an existing ID
        serviceDetails.setId(1L);

        int databaseSizeBeforeCreate = serviceDetailsRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restServiceDetailsMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(serviceDetails))
            )
            .andExpect(status().isBadRequest());

        // Validate the ServiceDetails in the database
        List<ServiceDetails> serviceDetailsList = serviceDetailsRepository.findAll();
        assertThat(serviceDetailsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllServiceDetails() throws Exception {
        // Initialize the database
        serviceDetailsRepository.saveAndFlush(serviceDetails);

        // Get all the serviceDetailsList
        restServiceDetailsMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(serviceDetails.getId().intValue())))
            .andExpect(jsonPath("$.[*].comments").value(hasItem(DEFAULT_COMMENTS)));
    }

    @Test
    @Transactional
    void getServiceDetails() throws Exception {
        // Initialize the database
        serviceDetailsRepository.saveAndFlush(serviceDetails);

        // Get the serviceDetails
        restServiceDetailsMockMvc
            .perform(get(ENTITY_API_URL_ID, serviceDetails.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(serviceDetails.getId().intValue()))
            .andExpect(jsonPath("$.comments").value(DEFAULT_COMMENTS));
    }

    @Test
    @Transactional
    void getNonExistingServiceDetails() throws Exception {
        // Get the serviceDetails
        restServiceDetailsMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewServiceDetails() throws Exception {
        // Initialize the database
        serviceDetailsRepository.saveAndFlush(serviceDetails);

        int databaseSizeBeforeUpdate = serviceDetailsRepository.findAll().size();

        // Update the serviceDetails
        ServiceDetails updatedServiceDetails = serviceDetailsRepository.findById(serviceDetails.getId()).get();
        // Disconnect from session so that the updates on updatedServiceDetails are not directly saved in db
        em.detach(updatedServiceDetails);
        updatedServiceDetails.comments(UPDATED_COMMENTS);

        restServiceDetailsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedServiceDetails.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedServiceDetails))
            )
            .andExpect(status().isOk());

        // Validate the ServiceDetails in the database
        List<ServiceDetails> serviceDetailsList = serviceDetailsRepository.findAll();
        assertThat(serviceDetailsList).hasSize(databaseSizeBeforeUpdate);
        ServiceDetails testServiceDetails = serviceDetailsList.get(serviceDetailsList.size() - 1);
        assertThat(testServiceDetails.getComments()).isEqualTo(UPDATED_COMMENTS);
    }

    @Test
    @Transactional
    void putNonExistingServiceDetails() throws Exception {
        int databaseSizeBeforeUpdate = serviceDetailsRepository.findAll().size();
        serviceDetails.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restServiceDetailsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, serviceDetails.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(serviceDetails))
            )
            .andExpect(status().isBadRequest());

        // Validate the ServiceDetails in the database
        List<ServiceDetails> serviceDetailsList = serviceDetailsRepository.findAll();
        assertThat(serviceDetailsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchServiceDetails() throws Exception {
        int databaseSizeBeforeUpdate = serviceDetailsRepository.findAll().size();
        serviceDetails.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restServiceDetailsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(serviceDetails))
            )
            .andExpect(status().isBadRequest());

        // Validate the ServiceDetails in the database
        List<ServiceDetails> serviceDetailsList = serviceDetailsRepository.findAll();
        assertThat(serviceDetailsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamServiceDetails() throws Exception {
        int databaseSizeBeforeUpdate = serviceDetailsRepository.findAll().size();
        serviceDetails.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restServiceDetailsMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(serviceDetails)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the ServiceDetails in the database
        List<ServiceDetails> serviceDetailsList = serviceDetailsRepository.findAll();
        assertThat(serviceDetailsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateServiceDetailsWithPatch() throws Exception {
        // Initialize the database
        serviceDetailsRepository.saveAndFlush(serviceDetails);

        int databaseSizeBeforeUpdate = serviceDetailsRepository.findAll().size();

        // Update the serviceDetails using partial update
        ServiceDetails partialUpdatedServiceDetails = new ServiceDetails();
        partialUpdatedServiceDetails.setId(serviceDetails.getId());

        restServiceDetailsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedServiceDetails.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedServiceDetails))
            )
            .andExpect(status().isOk());

        // Validate the ServiceDetails in the database
        List<ServiceDetails> serviceDetailsList = serviceDetailsRepository.findAll();
        assertThat(serviceDetailsList).hasSize(databaseSizeBeforeUpdate);
        ServiceDetails testServiceDetails = serviceDetailsList.get(serviceDetailsList.size() - 1);
        assertThat(testServiceDetails.getComments()).isEqualTo(DEFAULT_COMMENTS);
    }

    @Test
    @Transactional
    void fullUpdateServiceDetailsWithPatch() throws Exception {
        // Initialize the database
        serviceDetailsRepository.saveAndFlush(serviceDetails);

        int databaseSizeBeforeUpdate = serviceDetailsRepository.findAll().size();

        // Update the serviceDetails using partial update
        ServiceDetails partialUpdatedServiceDetails = new ServiceDetails();
        partialUpdatedServiceDetails.setId(serviceDetails.getId());

        partialUpdatedServiceDetails.comments(UPDATED_COMMENTS);

        restServiceDetailsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedServiceDetails.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedServiceDetails))
            )
            .andExpect(status().isOk());

        // Validate the ServiceDetails in the database
        List<ServiceDetails> serviceDetailsList = serviceDetailsRepository.findAll();
        assertThat(serviceDetailsList).hasSize(databaseSizeBeforeUpdate);
        ServiceDetails testServiceDetails = serviceDetailsList.get(serviceDetailsList.size() - 1);
        assertThat(testServiceDetails.getComments()).isEqualTo(UPDATED_COMMENTS);
    }

    @Test
    @Transactional
    void patchNonExistingServiceDetails() throws Exception {
        int databaseSizeBeforeUpdate = serviceDetailsRepository.findAll().size();
        serviceDetails.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restServiceDetailsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, serviceDetails.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(serviceDetails))
            )
            .andExpect(status().isBadRequest());

        // Validate the ServiceDetails in the database
        List<ServiceDetails> serviceDetailsList = serviceDetailsRepository.findAll();
        assertThat(serviceDetailsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchServiceDetails() throws Exception {
        int databaseSizeBeforeUpdate = serviceDetailsRepository.findAll().size();
        serviceDetails.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restServiceDetailsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(serviceDetails))
            )
            .andExpect(status().isBadRequest());

        // Validate the ServiceDetails in the database
        List<ServiceDetails> serviceDetailsList = serviceDetailsRepository.findAll();
        assertThat(serviceDetailsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamServiceDetails() throws Exception {
        int databaseSizeBeforeUpdate = serviceDetailsRepository.findAll().size();
        serviceDetails.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restServiceDetailsMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(serviceDetails))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ServiceDetails in the database
        List<ServiceDetails> serviceDetailsList = serviceDetailsRepository.findAll();
        assertThat(serviceDetailsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteServiceDetails() throws Exception {
        // Initialize the database
        serviceDetailsRepository.saveAndFlush(serviceDetails);

        int databaseSizeBeforeDelete = serviceDetailsRepository.findAll().size();

        // Delete the serviceDetails
        restServiceDetailsMockMvc
            .perform(delete(ENTITY_API_URL_ID, serviceDetails.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ServiceDetails> serviceDetailsList = serviceDetailsRepository.findAll();
        assertThat(serviceDetailsList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
