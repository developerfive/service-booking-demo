package com.mycompany.myapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ServiceDetailsTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ServiceDetails.class);
        ServiceDetails serviceDetails1 = new ServiceDetails();
        serviceDetails1.setId(1L);
        ServiceDetails serviceDetails2 = new ServiceDetails();
        serviceDetails2.setId(serviceDetails1.getId());
        assertThat(serviceDetails1).isEqualTo(serviceDetails2);
        serviceDetails2.setId(2L);
        assertThat(serviceDetails1).isNotEqualTo(serviceDetails2);
        serviceDetails1.setId(null);
        assertThat(serviceDetails1).isNotEqualTo(serviceDetails2);
    }
}
